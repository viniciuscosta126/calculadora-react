function CalculadoraService() {
  const SOMA = "+";
  const SUBTRAIR = "-";
  const DIVISAO = "/";
  const MULTIPLICACAO = "*";

  function calcular(n1, n2, operacao) {
    let resultado;
    switch (operacao) {
      case SOMA:
        resultado = n1 + n2;
        break;

      case SUBTRAIR:
        resultado = n1 - n2;
        break;

      case DIVISAO:
        resultado = n1 / n2;
        break;

      case MULTIPLICACAO:
        resultado = n1 * n2;
        break;

      default:
        resultado = 0;
        break;
    }
    return resultado;
  }

  function concatenarNumero(numAtual, numConcat) {
    //Caso contenha '0' ou null, reinicia o valor
    if (numAtual === "0" || numAtual === null) {
      numAtual = "";
    }
    //Primeiro digitou for '.', concatenar 0 antes do ponto
    if (numConcat === "." && numAtual === "") {
      return "0.";
    }
    //Caso '.' digitado e ja contenha um ponto apenas retorna
    if (numConcat === "." && numAtual.indexOf(".") > -1) {
      return numAtual;
    }

    return numAtual + numConcat;
  }

  return [calcular, concatenarNumero, SOMA, SUBTRAIR, DIVISAO, MULTIPLICACAO];
}

export default CalculadoraService;
