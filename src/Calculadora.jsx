import React, {useState} from "react";
import "./Calculadora.css";
import CalculadoraService from "./calculadora.service"
import { Container, Row, Col, Button, Form } from "react-bootstrap";

function Calculadora() {
  const [txtNumeros,setTxtNumeros] = useState('0')
  const [n1, setN1] = useState('0')
  const [n2,setN2] = useState(null)
  const [operacao, setOperacao] = useState(null)

  const [calcular, concatenarNumero, SOMA, SUBTRAIR, DIVISAO, MULTIPLICACAO] = CalculadoraService();

  function add_Numero(numero){
    let resultado;
    if(operacao === null){
      resultado = concatenarNumero(n1,numero)
      setN1(resultado)

    }else{
      resultado = concatenarNumero(n2,numero)
      setN2(resultado)
    }

    setTxtNumeros(resultado)
  }

  function def_operacao(op){
    //Apenas definir operacao caso nao exista
    if(operacao === null){
      setOperacao(op)
      return;
    }
    //Caso exista operacao e numero 2 selecionado, realiza calculo
    if(n2 !== null){
      const resultado = calcular(parseFloat(n1),parseFloat(n2), operacao)
      setOperacao(op)
      setN1(resultado.toString())
      setN2(null)
      setTxtNumeros(resultado.toString())
    }
  }

  function acaoCalcular(){
    if(n2 === null){
      return
    }

    const resultado = calcular(parseFloat(n1),parseFloat(n2),operacao)
    setTxtNumeros(resultado.toString())
  }

  function limpar(){
    setTxtNumeros('0')
    setN1('0')
    setN2(null)
    setOperacao(null)
  }
  return (
    <div
      style={{
        background: "transparent !important",
        backgroundColor: "#007bff",
        padding: "5px",
        margin: "5px",
        width: "400px",
        borderRadius:"5px"
      }}
    >
      <Container>
        <Row>
          <Col xs="3">
            <Button variant="danger" onClick={limpar}>C</Button>
          </Col>
          <Col xs="9">
            <Form.Control
              type="text"
              name="txtNumeros"
              className="text-right"
              readOnly="readonly"
              value={txtNumeros}
              data-testid="txtNumeros"
            ></Form.Control>
          </Col>
        </Row>
        <Row>
          <Col xs="3"><Button variant="light" onClick={()=>add_Numero(7)}>7</Button></Col>
          <Col xs="3"><Button variant="light" onClick={()=>add_Numero(8)}>8</Button></Col>
          <Col xs="3"><Button variant="light" onClick={()=>add_Numero(9)}>9</Button></Col>
          <Col xs="3"><Button variant="warning" onClick={()=>def_operacao(DIVISAO)}>/</Button></Col>  
        </Row>
        <Row>
          <Col xs="3"><Button variant="light" onClick={()=>add_Numero(4)}>4</Button></Col>
          <Col xs="3"><Button variant="light" onClick={()=>add_Numero(5)}>5</Button></Col>
          <Col xs="3"><Button variant="light" onClick={()=>add_Numero(6)}>6</Button></Col>
          <Col xs="3"><Button variant="warning" onClick={()=>def_operacao(MULTIPLICACAO)}>*</Button></Col>  
        </Row>
        <Row>
          <Col xs="3"><Button variant="light" onClick={()=>add_Numero(1)}>1</Button></Col>
          <Col xs="3"><Button variant="light" onClick={()=>add_Numero(2)}>2</Button></Col>
          <Col xs="3"><Button variant="light" onClick={()=>add_Numero(3)}>3</Button></Col>
          <Col xs="3"><Button variant="warning" onClick={()=>def_operacao(SUBTRAIR)}>-</Button></Col>  
        </Row>
        <Row>
          <Col xs="3"><Button variant="light" onClick={()=>add_Numero(0)}>0</Button></Col>
          <Col xs="3"><Button variant="light" onClick={()=>add_Numero('.')}>.</Button></Col>
          <Col xs="3"><Button variant="success" onClick={acaoCalcular}>=</Button></Col>
          <Col xs="3"><Button variant="warning" onClick={()=>def_operacao(SOMA)}>+</Button></Col>  
        </Row>
      </Container>
    </div>
  );
}

export default Calculadora;
