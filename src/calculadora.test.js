import React from "react";
import ReactDOM from "react-dom";
import Calculadora from "./Calculadora.jsx";
import { render, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

describe("Calculadora", () => {
  it("Deve renderizar componente na tela, sem erros", () => {
    const div = document.createElement("div");
    ReactDOM.render(<Calculadora />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it("Deve limpar o campo de numeros", () => {
    const { getByTestId, getByText } = render(<Calculadora />);
    fireEvent.click(getByText("2"));
    fireEvent.click(getByText("C"));
    expect(getByTestId("txtNumeros")).toHaveValue("0");
  });

  it("Deve somar 2 + 3 e obter 5", () => {
    const { getByTestId, getByText } = render(<Calculadora />);
    fireEvent.click(getByText("2"));
    fireEvent.click(getByText("+"));
    fireEvent.click(getByText("3"));
    fireEvent.click(getByText("="));
    expect(getByTestId("txtNumeros")).toHaveValue("5");
  });
  
  it("Deve subtrair 2 - 3 e obter -1", () => {
    const { getByTestId, getByText } = render(<Calculadora />);
    fireEvent.click(getByText("2"));
    fireEvent.click(getByText("-"));
    fireEvent.click(getByText("3"));
    fireEvent.click(getByText("="));
    expect(getByTestId("txtNumeros")).toHaveValue("-1");
  });

  it("Deve dividr 3 / 3 e obter 5", () => {
    const { getByTestId, getByText } = render(<Calculadora />);
    fireEvent.click(getByText("3"));
    fireEvent.click(getByText("/"));
    fireEvent.click(getByText("3"));
    fireEvent.click(getByText("="));
    expect(getByTestId("txtNumeros")).toHaveValue("1");
  });

  it("Deve multiplicar 3 * 3 e obter 5", () => {
    const { getByTestId, getByText } = render(<Calculadora />);
    fireEvent.click(getByText("3"));
    fireEvent.click(getByText("*"));
    fireEvent.click(getByText("3"));
    fireEvent.click(getByText("="));
    expect(getByTestId("txtNumeros")).toHaveValue("9");
  });

});
