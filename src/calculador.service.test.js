import React from "react";
import ReactDOM from "react-dom";
import CalculadoraService from "./calculadora.service";

describe("Teste do CalculadoraService", () => {
  const [calcular, concatenarNumero, SOMA, SUBTRAIR, DIVISAO, MULTIPLICACAO] =
    CalculadoraService();

  it("Deve garantir que 1 + 4 = 5", () => {
    let soma = calcular(1, 4, SOMA);
    expect(soma).toEqual(5);
  });

  it("Deve garantir que 1 - 4 = -3", () => {
    let subtrair = calcular(1, 4, SUBTRAIR);
    expect(subtrair).toEqual(-3);
  });

  it("Deve garantir que 4 / 2 = 2", () => {
    let divisao = calcular(4, 2, DIVISAO);
    expect(divisao).toEqual(2);
  });

  it("Deve garantir que 2 * 5 = 10", () => {
    let multiplicacao = calcular(2, 5, MULTIPLICACAO);
    expect(multiplicacao).toEqual(10);
  });

  it("Deve retornar 0 para operacao invalida", () => {
      let opInvalida = calcular(1, 2, "%");
      expect(opInvalida).toEqual(0)
  })

});
